<div id="topBackground"></div>
<div id="middleBackground"></div>
<div id="logo"></div>
<div id="modeSwitch">
  <!-- TODO: active class has to be set on really selected mode -->
  <jummp:button class="left active"><g:message code="jummp.main.search"/></jummp:button>
  <jummp:button class="right"><g:message code="jummp.main.submit"/></jummp:button>
</div>
