<sec:ifAnyGranted roles="ROLE_ADMIN">
  <wcm:widget path="widgets/admin"/>
</sec:ifAnyGranted>
<wcm:widget path="widgets/news-sidebar-item"/>
<wcm:widget path="widgets/sbml-feed"/>
