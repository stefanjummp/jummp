<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name='layout' content='main' />
        <title>Welcome to JUMMP</title>
        <g:javascript>
        $(document).ready(function() {
            showErrorMessage(i18n.error.denied);
        });
        </g:javascript>
    </head>

    <body>
        <div id="body" class="ui-widget"></div>
    </body>
</html>
