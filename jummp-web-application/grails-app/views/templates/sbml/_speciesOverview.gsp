<table>
    <sbml:speciesTitleTableRow title="${title}"/>
    <sbml:initialAmountTableRow initialAmount="${initialAmount}"/>
    <sbml:initialConcentrationTableRow initialConcentration="${initialConcentration}"/>
    <sbml:substanceUnitsTableRow substanceUnits="${substanceUnits}"/>
    <jummp:sboTableRow sbo="${sbo}"/>
    <jummp:annotationsTableRow annotations="${annotation}"/>
    <sbml:notesTableRow notes="${notes}"/>
</table>
