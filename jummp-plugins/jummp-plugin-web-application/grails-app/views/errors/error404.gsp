<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="main" />
        <title><g:message code="error.404.title"/></title>
    </head>
    <body>
        <h1><g:message code="error.404.title"/></h1>
        <p><g:message code="error.404.explanation" args="${[resource]}"/>
    </body>
</html>
