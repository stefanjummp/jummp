<button type="button" ${attrs.class ? 'class="' + attrs.class + '"' : ''} ${attrs.id ? 'id="' + attrs.id + '"' : ''}>
    <p>${body}</p>
    <div class="glow"></div>
</button>
