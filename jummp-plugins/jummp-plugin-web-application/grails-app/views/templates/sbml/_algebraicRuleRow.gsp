<tr>
    <th class="ruleTitle"><g:message code="sbml.rules.algebraicRule"/></th>
    <td class="ruleValue"><span><sbml:algebraicRuleMath><jummp:contentMathML mathML="${math}"/></sbml:algebraicRuleMath></span></td>
</tr>
