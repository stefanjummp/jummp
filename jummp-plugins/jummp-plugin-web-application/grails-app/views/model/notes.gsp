<g:if test="${notes}">
<div id="model-notes">
    <h2><g:message code="model.summary.notes"/></h2>
    <sbml:notes notes="${notes}"/>
</div>
</g:if>
