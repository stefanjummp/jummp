package net.biomodels.jummp.webapp
import net.biomodels.jummp.core.model.RevisionTransportCommand
import net.biomodels.jummp.core.model.PublicationTransportCommand

class ModelController {
    /**
     * Dependency injection of modelDelegateService.
     **/
    def modelDelegateService
    /**
     * Dependency injection of sbmlService.
     */
    def sbmlService

    def show = {
        [id: params.id]
    }

    /**
     * File download of the model file for a model by id
     */
    def download = {
        byte[] bytes = modelDelegateService.retrieveModelFile(new RevisionTransportCommand(id: params.id as int))
        response.setContentType("application/xml")
        // TODO: set a proper name for the model
        response.setHeader("Content-disposition", "attachment;filename=\"model.xml\"")
        response.outputStream << new ByteArrayInputStream(bytes)
    }

    def model = {
        RevisionTransportCommand rev = modelDelegateService.getLatestRevision(params.id as Long)
        List<String> authors = []
        rev.model.publication?.authors.each {
            authors.add("${it.firstName} ${it.lastName}, ")
        }
        if(!authors.empty) {
            String auth = authors.get(authors.size() - 1)
            authors.remove(authors.get(authors.size() - 1))
            authors.add(authors.size(), auth.substring(0, auth.length() - 2))
        }
        [revision: rev, authors: authors]
    }

    /**
     * Display basic information about the model
     */
    def summary = {
        RevisionTransportCommand rev = modelDelegateService.getLatestRevision(params.id as Long)
        [publication: modelDelegateService.getPublication(params.id as Long), revision: rev, notes: sbmlService.getNotes(rev), annotations: sbmlService.getAnnotations(rev)]
    }

    def overview = {
        RevisionTransportCommand rev = modelDelegateService.getLatestRevision(params.id as Long)
        [
                    reactions: sbmlService.getReactions(rev),
                    rules: sbmlService.getRules(rev),
                    parameters: sbmlService.getParameters(rev),
                    compartments: sbmlService.getCompartments(rev)
                ]
    }

    /**
     * Renders html snippet with Publication information for the current Model identified by the id.
     */
    def publication = {
        PublicationTransportCommand publication = modelDelegateService.getPublication(params.id as Long)
        [publication: publication]
    }

    def notes = {
        RevisionTransportCommand rev = modelDelegateService.getLatestRevision(params.id as Long)
        [notes: sbmlService.getNotes(rev)]
    }

    /**
     * Retrieve annotations and hand them over to the view
     */
    def annotations = {
        RevisionTransportCommand rev = modelDelegateService.getLatestRevision(params.id as Long)
        [annotations: sbmlService.getAnnotations(rev)]
    }

    /**
    * File download of the model file for a model by id
    */
   def downloadModelRevision = {
       RevisionTransportCommand rev = modelDelegateService.getLatestRevision(params.id as Long)
       byte[] bytes = modelDelegateService.retrieveModelFile(rev)
       response.setContentType("application/xml")
       // TODO: set a proper name for the model
       response.setHeader("Content-disposition", "attachment;filename=\"model.xml\"")
       response.outputStream << new ByteArrayInputStream(bytes)
   }
}
