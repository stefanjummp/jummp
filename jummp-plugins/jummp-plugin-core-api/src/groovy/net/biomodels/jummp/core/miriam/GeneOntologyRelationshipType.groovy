package net.biomodels.jummp.core.miriam

/**
 * @short Enum describing the type of a @link GeneOntologyRelationship.
 * 
 * @author Martin Gräßlin <m.graesslin@dkfz.de>
 */
public enum GeneOntologyRelationshipType {
    IsA,
    PartOf,
    DevelopFrom,
    Other
}
