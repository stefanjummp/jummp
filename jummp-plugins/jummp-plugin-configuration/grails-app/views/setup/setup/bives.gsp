<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Setup</title>
    </head>
    <body>
        <div id="remote" class="body">
            <h1>Model Versioning System - BiVeS</h1>
            <g:form name="bivesForm" action="setup">
                <g:render template="/templates/configuration/bives"/>
                <div class="buttons">
                    <g:submitButton name="back" value="Back"/>
                    <g:submitButton name="next" value="Finish"/>
                </div>
            </g:form>
        </div>
    </body>
</html>