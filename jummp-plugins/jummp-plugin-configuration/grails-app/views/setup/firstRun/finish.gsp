<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>First Run Wizard</title>
    </head>
    <body>
        <div id="remote" class="body yui-skin-sam">
            <h1>User created</h1>
            <div>
                <p style="margin-left: 12px;">
                    The admin user has been created. To be able to login you have to restart your web application.
                </p>
            </div>
        </div>
    </body>
</html>
