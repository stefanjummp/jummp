<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Configuration Saved</title>
    </head>
    <body>
        <div id="remote" class="body">
            <h1>Configuration Saved</h1>
            <p>The Configuration for ${module} has been saved successfully!</p>
            <p>The new settings will be activated after a restart of the web application.</p>
        </div>
    </body>
    <g:render template="/templates/configuration/configurationSidebar"/>
</html>
