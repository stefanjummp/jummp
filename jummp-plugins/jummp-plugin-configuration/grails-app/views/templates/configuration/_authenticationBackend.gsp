<div class="dialog">
    <table class="formtable">
        <tbody>
            <tr class="prop">
                <td class="name"><label for="database">Database:</label></td>
                <td class="value"><input type="radio" name="authenticationBackend" id="database" value="database" checked="checked"/></td>
            </tr>
            <tr class="prop">
                <td class="name"><label for="ldap">LDAP:</label></td>
                <td class="value"><input type="radio" name="authenticationBackend" id="ldap" value="ldap"/></td>
            </tr>
        </tbody>
    </table>
</div>
